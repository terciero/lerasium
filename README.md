# lerasium
An operating systerm agnostic Gitlab test environment using Docker Compose.

## NOTE
This project is for testing and exposure to Gitlab products only. This project is inherently insecure and should **never** be deployed in production or any other environment that is accessible to the general public.

## Usage
In order to deploy both the Gitlab web and gitlab runner containers run:

`docker-compose up`

If you would like to build the Gitlab web container alone run:

`docker-compose up web`

Once the container has deployed and the Gitlab service has started you will be able to access Gitlab at:

[http://localhost:8929](http://localhost:8929)

Login with the username `root` and password `notpassword`